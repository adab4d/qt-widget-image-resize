"""Adapt widget to image displayed via QSS
"""
import ipdb
import sys

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QDialog
from PySide2.QtCore import QFile, QIODevice


# Use filt and pfilt to browse through object properties while at pdb
def filt(widget, name=""):
    return [prop for prop in dir(widget) if name in prop.lower()]


def pfilt(widget, name):
    for prop in filt(widget, name):
        print(prop)


class MyWindow(QDialog):
    def __init__(self, ui_file_name="mainwindow.ui"):
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)

        loader = QUiLoader()
        self.ui = loader.load(ui_file)
        if not self.ui:
            print(loader.errorString())
            sys.exit(-1)

        self.ui.button.clicked.connect(self.on_button_clicked)
        self.ui.setStyleSheet("""
            QWidget[class="image"] {
                background-image: url(image.png);
                background-repeat: no-repeat;
            }

            QLabel {
                image: url(image.png);
            }
        """)
        self.ui.show()

        self.ui.show()
        ui_file.close()

    def on_button_clicked(self, is_clicked):
        print("enter debug console")
        ipdb.set_trace()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())
